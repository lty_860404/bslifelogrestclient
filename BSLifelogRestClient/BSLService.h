//
//  BSLService.h
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSLRestClient.h"

@interface BSLService : NSObject

@property (strong,nonatomic) BSLRestClient *restClient;

-(void) doActionAfterAllRestFinished:(void (^)(void))action;
    
@end
