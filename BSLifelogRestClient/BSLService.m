//
//  BSLService.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-5-15.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLService.h"

@implementation BSLService

-(void) doActionAfterAllRestFinished:(void (^)(void))action{
    [self.restClient doActionAfterAllRestFinished:action];
}

@end
