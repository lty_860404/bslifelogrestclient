//
//  BSLFileRouteManager.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFileRouteManager.h"
#import "BSLFileKey.h"
#import "RKObjectMapping.h"

@implementation BSLFileRouteManager

-(instancetype) initWithLoadDatas
{
    self = [super initWithLoadDatas];
    if(self!=nil){
        [self reloadDatas];
    }
    return self;
}

-(void) reloadDatas{
    [super reloadDatas];
    [self loadFileGetRest];
}

-(void) loadFileGetRest{
    //add route
    RKRoute *fileGetRoute = [RKRoute routeWithRelationshipName:@ROUTE_FILE_GET_ID
                                                   objectClass:[BSLFileKey class]
                                                   pathPattern:@ROUTE_FILE_GET_PATH
                                                        method:RKRequestMethodGET];
    [[super routeSet] addObject:fileGetRoute];
}

@end
