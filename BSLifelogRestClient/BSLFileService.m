//
//  BSLFileService.m
//  BSLifelogRestClient
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFileService.h"
#import "BSLSystemConfig.h"
#import "BSLFileKey.h"

@implementation BSLFileService

@synthesize restClient;

static BSLFileService *fileService = nil;

+(instancetype) getInstance{
    if(fileService==nil){
        BSLRestClient *client = [[BSLRestClient alloc] initWithBaseURL:[BSLSystemConfig baseURL]];
        BSLFileRouteManager* fileRouteMgr = [[BSLFileRouteManager alloc] initWithLoadDatas];
        [client loadRoutes:fileRouteMgr];
        fileService = [[BSLFileService alloc] init];
        [fileService setRestClient:client];
    }
    return fileService;
}

-(void) getFile:(NSString*) fileKey
        success:(void (^)(NSData *data))success
        failure:(void (^)(NSError *err))failure
{
    [restClient setAcceptHeaderWithMIMEType:RKMIMETypeTextXML];
    BSLFileKey *fileKeyParams = [[BSLFileKey alloc] init];
    [fileKeyParams setFileKey:fileKey];
    NSURL* url = [restClient.manager.router URLForRelationship:@ROUTE_FILE_GET_ID
                                                      ofObject:fileKeyParams
                                                        method:RKRequestMethodGET];
    [restClient getDataByPath:[url path]
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject) {
                          NSLog(@"File Get Success!Content:%@",operation.response.description);
                          NSData *data = [[NSData alloc] initWithBase64EncodedData:operation.responseData options:NSDataBase64DecodingIgnoreUnknownCharacters];
                          success(data);
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                          NSLog(@"File Get Error!");
                          failure(error);
                      }];
}


@end
