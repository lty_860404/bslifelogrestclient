//
//  BSLViewController.m
//  BSLifelogRestClient DEMO
//
//  Created by tianyin luo on 14-6-4.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLUserViewController.h"
#import "BSLUserService.h"
#import "BSLFileService.h"

@interface BSLUserViewController ()

@property (strong,nonatomic) BSLUserService* userService;

@end

@implementation BSLUserViewController

@synthesize userService;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    userService = [BSLUserService getInstance];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction) testUserVerify:(id)sender
{
    [userService verify:@"+86 13123456789"
                success:^(BSLSuccessMsg *successMsg) {
                    NSLog(@"isSuccess:%@",successMsg.isSuccess?@"Yes":@"No");
                }
                failure:^(BSLError *err) {
                    NSLog(@"errCode:%@",err.errorCode);
                    NSLog(@"errURL:%@",err.errorURL);
                    NSLog(@"errMsg:%@",err.errorMsg);
                }];
}

-(IBAction) testUserInfo:(id)sender
{
    [userService getUserInfoWithId:1
                           success:^(BSLUser *user) {
                               NSLog(@"user Info name:%@",user.name);
                               NSLog(@"user Info phone:%@",user.phone);
                           }
                           failure:^(BSLError *err) {
                               NSLog(@"errCode:%@",err.errorCode);
                               NSLog(@"errURL:%@",err.errorURL);
                               NSLog(@"errMsg:%@",err.errorMsg);
                           }];
    //这里有问题，大于100可以8
    [userService getUserInfoWithId:100
                           success:^(BSLUser *user) {
                               NSLog(@"user Info name:%@",user.name);
                               NSLog(@"user Info phone:%@",user.phone);
                           }
                           failure:^(BSLError *err) {
                               NSLog(@"errCode:%@",err.errorCode);
                               NSLog(@"errURL:%@",err.errorURL);
                               NSLog(@"errMsg:%@",err.errorMsg);
                           }];
    
}

-(IBAction) testUsersInfo:(id)sender
{
    [userService getUsersInfoSuccess:^(NSArray *usersInfoArr) {
        for(int i=0;i<usersInfoArr.count;i++){
            BSLUser* user = [usersInfoArr objectAtIndex:i];
            NSLog(@"user Id:%ld",user.id);
            NSLog(@"username:%@",user.username);
            NSLog(@"user phone:%@",user.phone);
        }
    } failure:^(BSLError *err) {
        NSLog(@"errCode:%@",err.errorCode);
        NSLog(@"errURL:%@",err.errorURL);
        NSLog(@"errMsg:%@",err.errorMsg);
    }];
}

-(IBAction) testUserRemoveAndAdd:(id)sender
{
    [userService delUserWithId:3
                       success:^(BSLSuccessMsg *successMsg) {
                           NSLog(@"isSuccess:%@",successMsg.isSuccess?@"Yes":@"No");
                       } failure:^(BSLError *err) {
                           NSLog(@"errCode:%@",err.errorCode);
                           NSLog(@"errURL:%@",err.errorURL);
                           NSLog(@"errMsg:%@",err.errorMsg);
                       }];
    BSLUserAddParams *user = [[BSLUserAddParams alloc] init];
    [user setVerifyCode:@"tc26"];
    [user setUsername:@"test0"];
    [user setPassword:@"test0"];
    [user setName:@"test0"];
    [user setPhone:@"13012345678"];
    [user setFamilyName:@"test_HOME"];
    [userService addUser:user
                 success:^(BSLSuccessMsg *successMsg) {
                     NSLog(@"isSuccess:%@",successMsg.isSuccess?@"Yes":@"No");
                 } failure:^(BSLError *err) {
                     NSLog(@"errCode:%@",err.errorCode);
                     NSLog(@"errURL:%@",err.errorURL);
                     NSLog(@"errMsg:%@",err.errorMsg);
                 }];
}


-(IBAction) testChangeAvatar:(id)sender
{
    UIImage* avatar = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: @"http://img.article.pchome.net/00/37/34/66/pic_lib/wm/Orb_Icons_019.JPG"]]];
    [userService changeAvator:avatar
                     filename:@"Orb_Icons_019.JPG"
                       userId:1
                      success:^(BSLData *data) {
                          NSLog(@"data:%@",data.data);
                      } failure:^(BSLError *err) {
                          NSLog(@"errCode:%@",err.errorCode);
                          NSLog(@"errURL:%@",err.errorURL);
                          NSLog(@"errMsg:%@",err.errorMsg);
                      }];
}


-(IBAction)	testUploadImage:(id)sender{
    RKObjectManager *mgr = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"http://192.168.1.102:8080"]];
    BSLUser *user = [[BSLUser alloc] init];
    [user setId:1];
    NSURL *url = [NSURL URLWithString:@"http://img.article.pchome.net/00/37/34/66/pic_lib/wm/Orb_Icons_019.JPG"];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    // Serialize the Article attributes then attach a file
    NSMutableURLRequest *request = [mgr multipartFormRequestWithObject:user
                                                                method:RKRequestMethodPOST
                                                                  path:@"/users/1/change_avatar"
                                                            parameters:nil
                                             constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                 [formData appendPartWithFileData:UIImagePNGRepresentation(image)
                                                                             name:@"avatar"
                                                                         fileName:@"photo.png"
                                                                         mimeType:@"image/png"];
                                             }];
    
    RKObjectMapping *dataMapping = [RKObjectMapping mappingForClass:[BSLData class]];
    [dataMapping addAttributeMappingsFromDictionary:@{
                                                      @"data":@"data"
                                                      }];
    NSIndexSet *successCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    RKResponseDescriptor *dataDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:dataMapping
                                                                                        method:RKRequestMethodPOST
                                                                                   pathPattern:nil
                                                                                       keyPath:nil
                                                                                   statusCodes:successCodes];
    
    [mgr addResponseDescriptor:dataDescriptor];
    
    RKObjectRequestOperation *operation =
    [mgr objectRequestOperationWithRequest:request
                                   success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                       NSLog(@"We object mapped the response with the following result: %@", mappingResult);
                                       BSLData *data = [mappingResult firstObject];
                                       NSLog(data.data);
                                   }
                                   failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                       NSLog(@"ERROR:%@",[error localizedDescription]);
                                       BSLError* errMsg = [[error.userInfo objectForKey:RKObjectMapperErrorObjectsKey] firstObject];
                                       NSLog(errMsg.errorCode);
                                       NSLog(errMsg.errorURL);
                                       NSLog(errMsg.errorMsg);
                                   }];
    
    [mgr enqueueObjectRequestOperation:operation];
}

@end
