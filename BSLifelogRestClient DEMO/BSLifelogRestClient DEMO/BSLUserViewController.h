//
//  BSLViewController.h
//  BSLifelogRestClient DEMO
//
//  Created by tianyin luo on 14-6-4.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLUserViewController : UIViewController

-(IBAction)testUserVerify:(id)sender;


-(IBAction)testUserInfo:(id)sender;


-(IBAction)testUsersInfo:(id)sender;


-(IBAction) testUserRemoveAndAdd:(id)sender;


-(IBAction) testChangeAvatar:(id)sender;

-(IBAction)	testUploadImage:(id)sender;


@end
