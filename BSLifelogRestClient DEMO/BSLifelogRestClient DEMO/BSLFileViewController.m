//
//  BSLFileViewController.m
//  BSLifelogRestClient DEMO
//
//  Created by tianyin luo on 14-6-9.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLFileViewController.h"
#import "BSLFileService.h"

@interface BSLFileViewController ()

    @property (strong,nonatomic) BSLFileService *fileService;

@end

@implementation BSLFileViewController

@synthesize imageView;
@synthesize fileService;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    fileService = [BSLFileService getInstance];
    
    
    NSData *data = [NSData dataWithContentsOfURL:
                    [NSURL URLWithString: @"http://img.article.pchome.net/00/37/34/66/pic_lib/wm/Orb_Icons_019.JPG"]];
    NSLog(@"content-length:%d",data.length);
    UIImage* image = [UIImage imageWithData:data];
    
    [imageView setImage:image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)testFileGet:(id)sender{
    [fileService getFile:@"11a6433d-9875-4135-9676-35bbe7b0caa2_avatar"
                 success:^(NSData *data) {
                     NSLog(@"%d",[data length]);
                     UIImage *image = [UIImage imageWithData:data];
                     [imageView setImage:image];
                 }
                 failure:^(NSError *err) {
                     NSLog(@"%@",[err description]);
                 }];
    
}

@end
